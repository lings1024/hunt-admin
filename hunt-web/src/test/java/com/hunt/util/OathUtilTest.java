package com.hunt.util;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: ouyangan
 * @Date : 2017/1/14
 */
public class OathUtilTest {
    private static final Logger log = LoggerFactory.getLogger(OathUtilTest.class);

    @Test
    public void md5Signature() throws Exception {

    }

    @Test
    public void generateOathCode() throws Exception {
        String randomStr= OathUtil.generateOathCode(5);
        log.debug("randomStr:{}",randomStr);
        Assert.assertTrue(randomStr.length()==5);
    }

}